import { API_SERVER } from './constants';

export const sendFile = (formData) => (
    API_SERVER.post('api/file', formData).then(res => res.data).catch(error => error.response)
);