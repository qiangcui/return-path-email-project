class DAL:
    def __init__(self):
        pass

    @staticmethod
    def process_file(files):
        result_email_array = []

        for tmp_file in files:
            result_email = {}
            for line in tmp_file.readlines():
                if line.startswith('From:'):
                    result_email['From'] = line.split(":", 1)[1].lstrip().rstrip()
                elif line.startswith('To:'):
                    result_email['To'] = line.split(":", 1)[1].lstrip().rstrip()
                elif line.startswith('Date:'):
                    result_email['Date'] = line.split(":", 1)[1].lstrip().rstrip()
                elif line.startswith('Subject:'):
                    result_email['Subject'] = line.split(":", 1)[1].lstrip().rstrip()
                elif line.startswith('Message-ID:'):
                    result_email['Message-ID'] = line.split(":", 1)[1].lstrip().rstrip()

            result_email_array.append(result_email)

        return result_email_array

