from flask import request, jsonify, Blueprint
from process_file import DAL

GET = 'GET'
POST = 'POST'
PUT = 'PUT'
DELETE = 'DELETE'
DAL = DAL()

api = Blueprint('api', __name__)


@api.route('/', methods=[GET])
def index():
    return "Welcome to project-email-flask-server!!!"


@api.route('/api/file', methods=[POST])
def process_file():
    files = request.files.getlist('email[]')
    result = DAL.process_file(files)
    return jsonify(result)


