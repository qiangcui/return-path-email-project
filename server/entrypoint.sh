#!/usr/bin/env bash

echo "Running app!"
uwsgi --http :1234 --wsgi-file wsgi.py --processes 1 --threads 1