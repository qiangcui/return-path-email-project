#!usr/bin/env python

# Create the wsgi entry point
import uwsgi

from server import application

if __name__ == '__main__':
    application.run()
